package cern.test;

import cern.nxcals.api.backport.client.service.LHCFillDataService;
import cern.nxcals.api.backport.client.service.ServiceBuilder;
import cern.nxcals.api.backport.domain.core.filldata.LHCFill;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NxCalsFillTester2 {
    static {
        System.setProperty("service.url",
                "http://cs-ccr-nxcals6.cern.ch:19093,http://cs-ccr-nxcals7.cern.ch:19093,http://cs-ccr-nxcals8.cern.ch:19093");
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(NxCalsFillTester2.class);
    private static final ServiceBuilder BUILDER = ServiceBuilder.getInstance();
    private static final LHCFillDataService LHC_FILL_DATA_SERVICE = BUILDER.createLHCFillService();

    private void extractFill(int fillNo) {
        LOGGER.info("Looking for fill no {}", fillNo);
        LHCFill oneFill = LHC_FILL_DATA_SERVICE.getLHCFillAndBeamModesByFillNumber(fillNo);
        LOGGER.info("Got fill {}", oneFill);
    }

    public static void main(String[] args) {
        NxCalsFillTester2 tester = new NxCalsFillTester2();
        tester.extractFill(6952);
    }
}
